#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getopt
import sys

from monkey.ioc.core import Registry

__author__ = 'Xavier ROY'


def populate(registry: Registry, hostname):
    print('Populating database using hostname: {}'.format(hostname))
    entry_service = registry.get('entry_service')
    entry_service.create(hostname, 'jenkins', 'http://jenkins.tncy.eu')
    entry_service.create(hostname, 'maven', 'http://maven.tncy.eu')
    entry_service.create(hostname, 'sonar', 'http://sonar.tncy.eu')
    entry_service.create(hostname, 'nexus', 'http://nexus.tncy.eu')
    entry_service.create(hostname, 'survey2017',
                         'https://docs.google.com/forms/d/e/1FAIpQLSdONNxoRuGIlzRm5ahkBLzKvrM3Hl_4JuZv8OqxCNDRv6ZmNw/viewform?usp=sf_link')


def _read_opts(argv):
    hostname = 'localhost'
    config_file = 'conf/populate.json'
    try:
        opts, args = getopt.getopt(sys.argv[1:], "?h:f:")
    except getopt.GetoptError:
        print('populate.py -h <hostname> -f <path to config file>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-?':
            print('populate.py -h <hostname> -f <path to config file>')
            sys.exit()
        elif opt in '-h':
            hostname = arg
        elif opt in '-f':
            config_file = arg
    return hostname, config_file


def _run(argv):
    hostname, config_file = _read_opts(argv)

    registry = Registry()
    registry.load(config_file)

    try:
        logging_conf_file = registry.get('logging_conf', 'logging_conf')
        logging.config.fileConfig(logging_conf_file)
    except FileNotFoundError as e:
        print(e)

    populate(registry, hostname)


if __name__ == '__main__':  # pragma: no coverage
    _run(sys.argv)
