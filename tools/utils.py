#!/usr/bin/env python

import random, traceback

from datetime import timedelta

import json
from json.decoder import JSONDecodeError


def random_percent():
    return int(random.random() * 100)


def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + timedelta(seconds=random_second)


def random_pos():
    latitude = float('{:.4f}'.format(random.uniform(30, 70)))
    longitude = float('{:.4f}'.format(random.uniform(-12, 35)))
    return latitude, longitude


def load_data(config_file, collection):
    try:
        config = open(config_file)
        data = json.loads(config.read())
        return data[collection]
    except IOError:
        traceback.print_exc()
    except TypeError:
        traceback.print_exc()
    except JSONDecodeError:
        traceback.print_exc()


