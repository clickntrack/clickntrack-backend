#!/usr/bin/python
# -*- coding: utf-8 -*-

import configparser
import getopt
import sys

from tools import utils

from monkey.ioc.core import Registry

__author__ = 'Xavier ROY'


def populate(registry: Registry, bulk_config_file):
    print('Populating database...')

    config = configparser.ConfigParser()
    config.read(bulk_config_file)
    for section in config.sections():
        service = registry.get('entry_service')

        input_file = config[section]['input_file']
        collection_key = config[section]['collection_key']
        print(input_file)
        data = utils.load_data(input_file, collection_key)
        for d in data:
            service.bulk_create(d)


def _read_opts(argv):
    config_file = 'conf/cnt.json'
    bulk_config_file = 'conf/populate.ini'
    try:
        opts, args = getopt.getopt(argv[1:], "?:f:i:")
    except getopt.GetoptError:
        print('populate.py -f <path to config file> -i <path to bulk load config file>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-?':
            print('populate.py -f <path to config file> -i <path to bulk load config file>')
            sys.exit()
        elif opt in '-f':
            config_file = arg
        elif opt in '-i':
            bulk_config_file = arg
    return config_file, bulk_config_file


def _run(argv):
    config_file, bulk_config_file = _read_opts(argv)

    registry = Registry()
    registry.load(config_file)

    populate(registry, bulk_config_file)


if __name__ == '__main__':  # pragma: no coverage
    _run(sys.argv)
