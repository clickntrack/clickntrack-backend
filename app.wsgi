import logging
import logging.config
import os
import sys

from monkey.ioc.core import Registry

from clickntrack.webservices import app as _application


def _setup_context():
    os.chdir(os.path.dirname(__file__))
    sys.path.append(os.path.dirname(__file__))

    work_dir_path = os.getcwd()

    config_file = '/var/local/clickntrack/conf/main.json'
    try:
        config_file = os.environ['CNT_CONF']
    except IndexError:
        pass

    registry = Registry()
    registry.load(config_file)

    try:
        logging_conf_file = registry.get('logging_conf', 'logging_conf')
        logging.config.fileConfig(logging_conf_file)
    except FileNotFoundError as e:
        print(e)

    logger = logging.getLogger('wsgi')
    logger.info('Working directory: {}'.format(work_dir_path))
    logger.info('Config file: {}'.format(config_file))

    from clickntrack import webservices
    webservices.registry = registry

    logger.info('Starting Click & Track in mod_wsgi...')


def application(environ, start_response):
    return _application(environ, start_response)


# This loads the Bottle application
_setup_context()
