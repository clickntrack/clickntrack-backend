@echo off
cls

setlocal EnableDelayedExpansion
set script=%~n0
set parent=%~dp0

if "%1"=="test" set REPOSITORY_URL="https://test.pypi.org/legacy/"

:PYPI
if exist %USERPROFILE%\pypi.cmd call %USERPROFILE%\pypi.cmd
if defined PYPI_USER goto VENV
set /P PYPI_USER=PyPi user:
set /P PYPI_PWD=PyPi password:

:VENV
if defined VIRTUAL_ENV goto RUN

set LOCAL_VIRTUAL_ENV=1
call .\.venv\Scripts\activate.bat

:RUN
if defined REPOSITORY_URL set OPTIONS=--repository-url %REPOSITORY_URL%

python -m twine upload %OPTIONS% dist/* -u %PYPI_USER% -p %PYPI_PWD% --verbose

if not defined LOCAL_VIRTUAL_ENV goto END

call .\.venv\Scripts\deactivate.bat

endlocal

:END
